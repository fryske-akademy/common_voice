# egrep '(Pos.NOUN|Pos.VERB)' preferred.csv |awk 'BEGIN {FS="\t"} {print $3 "\t" $1}'|sort|uniq > nounsverbs.tsv

{
    for (i = 1; i <= NF; i++) {
        if (match($i,/'/)) continue;
        g = gensub(/[,:;.?!$]/, "", "g", $i)
        while ("grep '^" g "\\\s' nounsverbs.tsv |cut -f2" | getline alt) {
            printAlt($0,g,alt)
        }
        close("grep '^" g "\\\s' nounsverbs.tsv |cut -f2")
    }
}
function printAlt(line,search,replace,    g) {
    g = gensub(search, replace, "g", line)
    print "(" search " => " replace ")\t" g
}